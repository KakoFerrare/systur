﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysTur.CustomValidation
{
    class ValidationCpfCnpj : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value.ToString().Length <= 14)
            {
                this.ErrorMessage = "Número de CPF Inválido";
            }
            else
            {
                this.ErrorMessage = "Número de CNPJ Inválido";
            }
            return new CN.CNFuncoes().ValidarDocumento(value.ToString());
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentCulture,
              ErrorMessage, name);
        }
    }
}
