﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysTur.Model
{
    public class Rota
    {
        public int IdRota { get; set; }
        public DateTime? Data { get; set; }
        public int? IdAgencia { get; set; }
        public int? IdMotorista { get; set; }
        public int? IdEmp_hora { get; set; }
        public int? IdCarro { get; set; }
        public string Tipo { get; set; }
        public string Iv { get; set; }
        public int? km_inicial { get; set; }
        public DateTime? Hr_inicial { get; set; }
        public int? Qtde_pessoa { get; set; }
        public string TipoPessoa { get; set; }
        public int? Km_final { get; set; }
        public DateTime? Hr_final { get; set; }
        public DateTime? Dt_inc { get; set; }
        public string Obs { get; set; }
        public int Dt_excl { get; set; }
    }
}
