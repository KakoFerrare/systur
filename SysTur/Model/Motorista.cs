﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysTur.Model
{
    public class Motorista
    {
        public int IdMotorista { get; set; }
        public int? IdAgencia { get; set; }
        public string Nome { get; set; }
        public string CNH { get; set; }
        public string IdCpf { get; set; }
        public string Rg { get; set; }
        public DateTime? Dt_nasc { get; set; }
        public string Endereco { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string UF { get; set; }
        public string Cep { get; set; }
        public string Fone1 { get; set; }
        public string Fone2 { get; set; }
        public string Fone3 { get; set; }
        public string EstCivil { get; set; }
        public DateTime? Dt_valCnh { get; set; }
        public string Nome_abrev { get; set; }
        public string CnhCat { get; set; }
        public DateTime? Dt_admis { get; set; }
        public DateTime? Dt_demis { get; set; }
        public string Funcao { get; set; }
        public decimal? Salario { get; set; }
        public decimal? Beneficio { get; set; }
        public string Foto { get; set; }
        public DateTime? Dt_alt { get; set; }
        public int? IdUsuario_alt { get; set; }
        public DateTime? Dt_excl { get; set; }
        public int? IdUsuario_excl { get; set; }
    }
}
