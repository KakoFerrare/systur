﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysTur.Model
{
    public class UsuarioPermissao
    {
        public int IdAgencia { get; set; }
        public int IdNivel { get; set; }
        public int IdForm { get; set; }
        public int? Adicionar { get; set; }
        public int? Alterar { get; set; }
        public int? Remover { get; set; }
        public DateTime? Dt_inc { get; set; }
        public int? IdUsuario_inc { get; set; }
        public DateTime? Dt_alt { get; set; }
        public int? IdUsuario_alt { get; set; }
        public DateTime? Dt_excl { get; set; }
        public int? IdUsuario_excl { get; set; }
    }
}
