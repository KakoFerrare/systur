﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysTur.Model
{
    public class Manutencao
    {
        public int IdManutencao { get; set; }
        public int? IdCarro { get; set; }
        public string Descricao { get; set; }
        public int? Km_atual { get; set; }
        public int? Km_proximo { get; set; }
        public int? Km_alerta { get; set; }
        public string Ativo { get; set; }
        public DateTime? Dt_alt { get; set; }
        public int? IdUsuario_alt { get; set; }
        public DateTime? Dt_excl { get; set; }
        public int? IdUsuario_excl { get; set; }
    }
}
