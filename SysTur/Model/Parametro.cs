﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysTur.Model
{
    public class Parametro
    {
        public int IdGrupo { get; set; }
        public string IdLista { get; set; }
        public string Descricao { get; set; }
        public string Ativo { get; set; }
    }
}
