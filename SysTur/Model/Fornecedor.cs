﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysTur.Model
{
    public class Fornecedor
    {
        public int IdFornecedor { get; set; }
        public int? IdAgencia { get; set; }
        public string Nome { get; set; }
        public string Fantasia { get; set; }
        public string Endereco { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string UF { get; set; }
        public string Cep { get; set; }
        public string Fone1 { get; set; }
        public string Fone2 { get; set; }
        public string Fone3 { get; set; }
        public string IdCpf { get; set; }
        public string Insc_Rg { get; set; }
        public string Email { get; set; }
        public string Contato { get; set; }
        public DateTime? Dt_inc { get; set; }
        public int? IdUsuario_inc { get; set; }
        public DateTime? Dt_alt { get; set; }
        public int? IdUsuario_alt { get; set; }
        public DateTime? Dt_excl { get; set; }
        public int? IdUsuario_excl { get; set; }
    }
}
