﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysTur.Model
{
    public class Agencia
    {
        public int IdAgencia { get; set; }
        public string Nome { get; set; }
        public string Ident { get; set; }
        public string Ativa { get; set; }
        public string Endereco { get; set; }
        public string Numero { get; set; }
        public string Cidade { get; set; }
        public string UF { get; set; }
        public string Cep { get; set; }
        public string RazaoSocial { get; set; }
        public string Cnpj { get; set; }
        public string IE { get; set; }
        public string Bairro { get; set; }
    }
}
