﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysTur.Model
{
    public class Coodenada
    {
        public int Id { get; set; }
        public string Device { get; set; }
        public Double Lat { get; set; }
        public Double Longt { get; set; }
        public Double Accur { get; set; }
        public DateTime Dt_device { get; set; }
        public DateTime Dt_inc { get; set; }
    }
}
