﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysTur.Model
{
    public class Viagem
    {
        public int IdViagens { get; set; }
        public int? IdEmpresa { get; set; }
        public int? IdCarro { get; set; }
        public int? IdMotorista { get; set; }
        public DateTime? Data { get; set; }
        public decimal? VlViagem { get; set; }
        public decimal? VlMolt { get; set; }
        public DateTime? Hora { get; set; }
        public string Realizada { get; set; }
        public string End_dest { get; set; }
        public string N_dest { get; set; }
        public string Cidade_dest { get; set; }
        public string Uf_dest { get; set; }
        public string Cep_dest { get; set; }
        public DateTime? Hora_final { get; set; }
        public DateTime? Data_final { get; set; }
        public int Diaria { get; set; }
        public string Obs { get; set; }
        public DateTime? Dt_inc { get; set; }
        public int? IdUsuario_inc { get; set; }
        public DateTime? Dt_alt { get; set; }
        public int? IdUsuario_alt { get; set; }
        public DateTime? Dt_excl { get; set; }
        public int? IdUsuario_excl { get; set; }
    }
}
