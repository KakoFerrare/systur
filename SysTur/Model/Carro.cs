﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysTur.Model
{
    public class Carro
    {
        public int IdCarro { get; set; }
        public int? IdAgencia { get; set; }
        public string Placa { get; set; }
        public string Marca { get; set; }
        public int? Km_atual { get; set; }
        public string Modelo { get; set; }
        public string Tipo { get; set; }
        public string Cor { get; set; }
        public string Anomod { get; set; }
        public string Combustivel { get; set; }
        public string Certificado { get; set; }

        public string Renavam { get; set; }
        public string Chassi { get; set; }
        public string Anofab { get; set; }
        public int? N_veic { get; set; }
        public DateTime? Venc_tac { get; set; }

        public DateTime? Dt_alt { get; set; }
        public int? IdUsuario_inc { get; set; }
        public DateTime? Dt_excl { get; set; }
        public int? IdUsuario_alt { get; set; }
        public DateTime? Dt_inc { get; set; }
        public int? IdUsuario_excl { get; set; }
    }
}
