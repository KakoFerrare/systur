﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysTur.Model
{
    public class Usuario
    {
        public int idUsuario { get; set; }
        public int? IdAgencia { get; set; }
        public string IdCpf { get; set; }
        public string Nome { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public string Ativo { get; set; }
        public Int16 ? Nivel { get; set; }
        public DateTime? Dt_inc { get; set; }
        public int? IdUsuario_inc { get; set; }
        public DateTime? Dt_alt { get; set; }
        public int? IdUsuario_alt { get; set; }
        public DateTime? Dt_excl { get; set; }
        public int? IdUsuario_excl { get; set; }
    }
}
