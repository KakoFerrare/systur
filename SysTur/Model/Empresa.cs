﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SysTur.CustomValidation;

namespace SysTur.Model
{
    public class Empresa : PropertyValidateModel
    {
        public int IdEmpresa { get; set; }
        [Required(ErrorMessage = "Selecione uma Agência")]
        public int? IdAgencia { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        [Display(Name = "Nome Fantásia")]
        public string Fantasia { get; set; }
        [Required]
        [Display(Name = "Endereço")]
        public string Endereco { get; set; }
        [Required]
        [RegularExpression("([1-9]+)", ErrorMessage = "Informe Apenas Números")]
        [Display(Name = "Número")]
        public string Numero { get; set; }
        public string Complemento { get; set; }
        [Required]
        public string Bairro { get; set; }
        [Required]
        public string Cidade { get; set; }
        [Required]
        [MaxLength(2)]
        [Display(Name = "Estado")]
        [MinLength(2)]
        [RegularExpression("^[ a-zA-Z á]*$", ErrorMessage = "Informe Apenas Letras")]
        public string UF { get; set; }
        [Required]
        [RegularExpression("[0-9]{5}-[0-9]{3}", ErrorMessage = "Formato válido: #####-###")]
        public string Cep { get; set; }
        [Required]
        [RegularExpression("([0-9]+)", ErrorMessage = "Informe Apenas Números")]
        [Display(Name = "1º Telefone")]
        public string Fone1 { get; set; }
        [Display(Name = "º Telefone")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Informe Apenas Números")]
        public string Fone2 { get; set; }
        [Display(Name = "3º Telefone")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Informe Apenas Números")]
        public string Fone3 { get; set; }
        [Required]
        [ValidationCpfCnpj]
        [MinLength(11, ErrorMessage ="Mínimo 11 Caracteres"),MaxLength(18, ErrorMessage = "Máximo 18 Caracteres")]
        [Display(Name = "Documento")]
        public string idCpf { get; set; }
        [Required]
        public string Insc_rg { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "E-mail")]
        public string Email { get; set; }
        [Required]
        public string Contato { get; set; }
        public string Tipo { get; set; }
        [Required]
        [Display(Name = "Data Nascimento")]
        public DateTime? Dt_nasc { get; set; }
        public string Ativo { get; set; }
        public DateTime? Dt_inc { get; set; }
        public int? IdUsuario_inc { get; set; }
        public DateTime? Dt_alt { get; set; }
        public int? IdUsuario_alt { get; set; }
        public DateTime? Dt_excl { get; set; }
        public int? IdUsuario_excl { get; set; }
    }
}
