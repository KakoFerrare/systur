﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysTur.Model
{
    
    public class Abastecimento : PropertyValidateModel
    {
        public int IdAbastecimento { get; set; }
        public int IdAgencia { get; set; }
        public int IdCarro { get; set; }
        public int IdFornecedor { get; set; }
        public int IdMotorista { get; set; }
        public int ? Km_inicial { get; set; }
        public int ? Km_final { get; set; }
        public int ? Km_total { get; set; }
        public decimal ? Litros_qtd { get; set; }
        public decimal ? Litros_valor { get; set; }
        public decimal ? Valor_total { get; set; }
        public DateTime ? Dt_alt { get; set; }
        public DateTime ? Dt_excl { get; set; }
        public DateTime ? Dt_inc { get; set; }
    }
}
