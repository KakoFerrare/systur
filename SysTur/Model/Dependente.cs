﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysTur.Model
{
    public class Dependente
    {
        public int IdDep { get; set; }
        public int? IdMotorista { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string RG { get; set; }
        public DateTime? Dt_nas { get; set; }
        public string Tipo { get; set; }
        public string Plano { get; set; }
        public string N_Cart { get; set; }
        public DateTime? Validade { get; set; }
    }
}
