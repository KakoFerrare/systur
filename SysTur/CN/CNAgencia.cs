﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SysTur.DAO;
using SysTur.Model;

namespace SysTur.CN
{
    public class CNAgencia
    {
        DAOAgencia DAOAgencia = new DAOAgencia();
        public List<Agencia> Listar()
        {
            return this.DAOAgencia.Listar();
        }

        public Agencia Carregar(int idAgencia)
        {
            return this.DAOAgencia.Carregar(idAgencia);
        }

    }
}
