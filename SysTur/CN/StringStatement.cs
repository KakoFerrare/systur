﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysTur.CN
{
    public class StringStatement
    {
        public string FormatCPF(string cpf)
        {
            if (!string.IsNullOrEmpty(cpf))
                cpf = cpf.Insert(3, ".").Insert(7, ".").Insert(11, "-");
            return cpf;
        }

        public string FormatCNPJ(string cnpj)
        {
            if (!string.IsNullOrEmpty(cnpj))
                cnpj = cnpj.Insert(2, ".").Insert(6, ".").Insert(10, "/").Insert(15, "-");
            return cnpj;
        }

        public string FormatDocumento(string documento)
        {
            if (!string.IsNullOrEmpty(documento))
            {
                if (documento.Length == 11)
                {
                    documento = FormatCPF(documento);
                }
                else if (documento.Length == 14)
                {
                    documento = FormatCNPJ(documento);
                }
            }
            return documento;
        }
    }
}
