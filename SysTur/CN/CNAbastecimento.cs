﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SysTur.DAO;
using SysTur.Model;

namespace SysTur.CN
{
    public class CNAbastecimento
    {
        DAOAbastecimento DAOAbastecimento = new DAOAbastecimento();
        public List<Abastecimento> Listar(int? idAgencia = null, int? idCarro = null, int? idMotorista = null, int? idFornecedor = null)
        {
            return this.DAOAbastecimento.Listar(idAgencia, idCarro, idMotorista, idFornecedor);
        }

        public Abastecimento Carregar(int idAbastecimento)
        {
            return this.DAOAbastecimento.Carregar(idAbastecimento);
        }
    }
}
