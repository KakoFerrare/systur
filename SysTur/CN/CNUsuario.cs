﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SysTur.DAO;
using SysTur.Model;

namespace SysTur.CN
{
    public class CNUsuario
    {
        DAOUsuario DAOUsuario = new DAOUsuario();
        public Usuario Login(string nome, string senha)
        {
           return  DAOUsuario.Login(nome, senha);
        }
    }
}
