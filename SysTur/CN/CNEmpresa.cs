﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SysTur.DAO;
using SysTur.Model;

namespace SysTur.CN
{
    public class CNEmpresa
    {
        
        DAOEmpresa DAOEmpresa = new DAOEmpresa();
        public List<Empresa> Listar(int? idAgencia = null, int? idCarro = null, int? idMotorista = null, int? idFornecedor = null)
        {
            return this.DAOEmpresa.Listar();
        }

        public Empresa Carregar(int idEmpresa)
        {
            Empresa empresa = this.DAOEmpresa.Carregar(idEmpresa);
            empresa.Cep = empresa.Cep.Insert(5, "-");
            return empresa;
        }

        public string[] Delete(Empresa empresa)
        {
            return this.DAOEmpresa.Delete(empresa);
        }

        internal string[] Manage(Empresa empresa)
        {
            if(empresa.IdEmpresa == 0)
                return this.DAOEmpresa.Salvar(empresa);
            else
               return this.DAOEmpresa.Editar(empresa);
           
        }

        //public List<Empresa> Paginate(int page, int totalPerPage)
        //{
        //    return this.DAOEmpresa.Paginate(page, totalPerPage);
        //}

        //public int Registro()
        //{
        //    return this.DAOEmpresa.Registro();
        //}
    }
}
