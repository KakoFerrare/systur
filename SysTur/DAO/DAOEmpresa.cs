﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using SysTur.Model;

namespace SysTur.DAO
{
    public class DAOEmpresa
    {
        private StringBuilder strQuery = new StringBuilder();
        private string table = "tbEmpresas";
        private MySqlConnection conn = new MySqlConnection();
        private string connection = Properties.Settings.Default.connection;

        public List<Empresa> Listar()
        {
            strQuery = new StringBuilder();
            List<Empresa> Lista = new List<Empresa>();
            strQuery.Append(" SELECT IdEmpresa, IdAgencia, Nome, Fantasia, Endereco, Numero, Complemento, Bairro, Cidade, UF, CEP, Fone1, Fone2, Fone3, idCpf, Insc_rg, Email, Contato, Tipo, Dt_nasc, Ativo, Dt_inc, IdUsuario_inc, Dt_alt, IdUsuario_alt, Dt_excl, IdUsuario_excl FROM " + table);

            using (conn = new MySqlConnection(connection))
            {
                conn.Open();
                try
                {
                    using (MySqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = strQuery.ToString();
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Empresa Empresa = new Empresa();
                                foreach (PropertyInfo propertyInfo in Empresa.GetType().GetProperties())
                                {
                                    if (propertyInfo.Name != "Error" && propertyInfo.Name != "Item")
                                        propertyInfo.SetValue(Empresa, reader[propertyInfo.Name] == DBNull.Value ? null : reader[propertyInfo.Name], null);
                                    if (propertyInfo.Name == "idCpf")
                                        Empresa.idCpf = new CN.StringStatement().FormatDocumento(Empresa.idCpf);
                                }
                                Lista.Add(Empresa);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Lista = new List<Empresa>();
                    MessageBox.Show("Erro ao carregar itens: " + e.Message, "Erro em execução", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            return Lista;
        }

        internal string[] Editar(Empresa empresa)
        {
            string[] retorno = new string[2];
            strQuery = new StringBuilder();
            strQuery.Append(" update " + table)
            .Append(" SET IdAgencia = @IdAgencia, Nome = @Nome, Fantasia = @Fantasia, Endereco = @Endereco, Numero = @Numero, Complemento = @Complemento, Bairro = @Bairro, Cidade = @Cidade, UF = @UF, CEP = @CEP, Fone1 = @Fone1, Fone2 = @Fone2, Fone3 = @Fone3, idCpf = @idCpf, Insc_rg = @Insc_rg, Email = @Email,")
            .Append("Contato = @Contato, Tipo = @Tipo, Dt_nasc = @Dt_nasc, Ativo = @Ativo, Dt_inc = @Dt_inc, IdUsuario_inc = @IdUsuario_inc, Dt_alt = @Dt_alt, IdUsuario_alt = @IdUsuario_alt, Dt_excl = @Dt_excl, IdUsuario_excl = @IdUsuario_excl ")
            .Append(" WHERE IdEmpresa = @idEmpresa");
            using (conn = new MySqlConnection(connection))
            {
                conn.Open();
                try
                {
                    using (MySqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = strQuery.ToString();
                        cmd.Parameters.AddWithValue("@idEmpresa", empresa.IdEmpresa);
                        cmd.Parameters.AddWithValue("@IdAgencia", empresa.IdAgencia);
                        cmd.Parameters.AddWithValue("@Nome", empresa.Nome);
                        cmd.Parameters.AddWithValue("@Fantasia", empresa.Fantasia);
                        cmd.Parameters.AddWithValue("@Endereco", empresa.Endereco);
                        cmd.Parameters.AddWithValue("@Numero", empresa.Numero);
                        cmd.Parameters.AddWithValue("@Complemento", empresa.Complemento);
                        cmd.Parameters.AddWithValue("@Bairro", empresa.Bairro);
                        cmd.Parameters.AddWithValue("@Cidade", empresa.Cidade);
                        cmd.Parameters.AddWithValue("@UF", empresa.UF);
                        cmd.Parameters.AddWithValue("@CEP", empresa.Cep.Replace("-", ""));
                        cmd.Parameters.AddWithValue("@Fone1", empresa.Fone1.Replace("(", "").Replace(")", "").Replace("-", ""));
                        cmd.Parameters.AddWithValue("@Fone2", empresa.Fone2.Replace("(", "").Replace(")", "").Replace("-", ""));
                        cmd.Parameters.AddWithValue("@Fone3", empresa.Fone3.Replace("(", "").Replace(")", "").Replace("-", ""));
                        cmd.Parameters.AddWithValue("@idCpf", empresa.idCpf.Replace(".", "").Replace("/", "").Replace("-", ""));
                        cmd.Parameters.AddWithValue("@Insc_rg", empresa.Insc_rg.Replace(".", "").Replace("/", "").Replace("-", ""));
                        cmd.Parameters.AddWithValue("@Email", empresa.Email);
                        cmd.Parameters.AddWithValue("@Ativo", empresa.Ativo);
                        cmd.Parameters.AddWithValue("@Contato", empresa.Contato);
                        cmd.Parameters.AddWithValue("@Tipo", (empresa.idCpf.Replace(".", "").Replace("/", "").Replace("-", "").Length > 11 ? "F" : "J"));
                        cmd.Parameters.AddWithValue("@Dt_nasc", empresa.Dt_nasc);
                        cmd.Parameters.AddWithValue("@Dt_inc", empresa.Dt_inc);
                        cmd.Parameters.AddWithValue("@IdUsuario_inc", empresa.IdUsuario_inc);
                        cmd.Parameters.AddWithValue("@Dt_alt", empresa.Dt_alt);
                        cmd.Parameters.AddWithValue("@IdUsuario_alt", empresa.IdUsuario_alt);
                        cmd.Parameters.AddWithValue("@Dt_excl", empresa.Dt_excl);
                        cmd.Parameters.AddWithValue("@IdUsuario_excl", empresa.IdUsuario_excl);
                        retorno[0] = cmd.ExecuteNonQuery().ToString();
                        if (Convert.ToInt16(retorno[0]) > 0)
                        {
                            retorno[1] = "Empresa " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(empresa.Nome.ToLower()) + " foi alterada";
                        }
                        else
                        {
                            retorno[1] = "Erro ao alterar a empresa " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(empresa.Nome.ToLower());
                        }
                    }
                }
                catch (Exception e)
                {
                    retorno[1] = "Erro ao alterar a empresa: " + e.Message;
                    retorno[0] = "0";
                }
                conn.Close();
            }

            return retorno;
        }

        internal string[] Salvar(Empresa empresa)
        {
            string[] retorno = new string[2];
            strQuery = new StringBuilder();
            strQuery.Append(" insert into " + table)
            .Append("(IdAgencia, Nome, Fantasia, Endereco, Numero, Complemento, Bairro, Cidade, UF, CEP, Fone1, Fone2, Fone3, idCpf, Insc_rg, Email, Contato, Tipo, Dt_nasc, Ativo, Dt_inc, IdUsuario_inc, Dt_alt, IdUsuario_alt, Dt_excl, IdUsuario_excl)")
            .Append("values")
            .Append("(@IdAgencia, @Nome, @Fantasia, @Endereco, @Numero, @Complemento, @Bairro, @Cidade, @UF, @CEP, @Fone1, @Fone2, @Fone3, @idCpf, @Insc_rg, @Email, @Contato, @Tipo, @Dt_nasc, @Ativo, @Dt_inc, @IdUsuario_inc, @Dt_alt, @IdUsuario_alt, @Dt_excl, @IdUsuario_excl)");
            using (conn = new MySqlConnection(connection))
            {
                conn.Open();
                try
                {
                    using (MySqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = strQuery.ToString();
                        cmd.Parameters.AddWithValue("@IdAgencia", empresa.IdAgencia);
                        cmd.Parameters.AddWithValue("@Nome", empresa.Nome);
                        cmd.Parameters.AddWithValue("@Fantasia", empresa.Fantasia);
                        cmd.Parameters.AddWithValue("@Endereco", empresa.Endereco);
                        cmd.Parameters.AddWithValue("@Numero", empresa.Numero);
                        cmd.Parameters.AddWithValue("@Complemento", empresa.Complemento);
                        cmd.Parameters.AddWithValue("@Bairro", empresa.Bairro);
                        cmd.Parameters.AddWithValue("@Cidade", empresa.Cidade);
                        cmd.Parameters.AddWithValue("@UF", empresa.UF);
                        cmd.Parameters.AddWithValue("@CEP", empresa.Cep.Replace("-", ""));
                        cmd.Parameters.AddWithValue("@Fone1", empresa.Fone1.Replace("(", "").Replace(")", "").Replace("-", ""));
                        cmd.Parameters.AddWithValue("@Fone2", empresa.Fone2.Replace("(", "").Replace(")", "").Replace("-", ""));
                        cmd.Parameters.AddWithValue("@Fone3", empresa.Fone3.Replace("(", "").Replace(")", "").Replace("-", ""));
                        cmd.Parameters.AddWithValue("@idCpf", empresa.idCpf.Replace(".", "").Replace("/", "").Replace("-", ""));
                        cmd.Parameters.AddWithValue("@Insc_rg", empresa.Insc_rg.Replace(".", "").Replace("/", "").Replace("-", ""));
                        cmd.Parameters.AddWithValue("@Email", empresa.Email);
                        cmd.Parameters.AddWithValue("@Ativo", 0);
                        cmd.Parameters.AddWithValue("@Contato", empresa.Contato);
                        cmd.Parameters.AddWithValue("@Tipo", (empresa.idCpf.Replace(".", "").Replace("/", "").Replace("-", "").Length == 11 ? "F" : "J"));
                        cmd.Parameters.AddWithValue("@Dt_nasc", empresa.Dt_nasc);
                        cmd.Parameters.AddWithValue("@Dt_inc", empresa.Dt_inc);
                        cmd.Parameters.AddWithValue("@IdUsuario_inc", empresa.IdUsuario_inc);
                        cmd.Parameters.AddWithValue("@Dt_alt", empresa.Dt_alt);
                        cmd.Parameters.AddWithValue("@IdUsuario_alt", empresa.IdUsuario_alt);
                        cmd.Parameters.AddWithValue("@Dt_excl", empresa.Dt_excl);
                        cmd.Parameters.AddWithValue("@IdUsuario_excl", empresa.IdUsuario_excl);
                        retorno[0] = cmd.ExecuteNonQuery().ToString();
                        if (Convert.ToInt16(retorno[0]) > 0)
                        {
                            retorno[1] = "Empresa " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(empresa.Nome.ToLower()) + " foi cadastrada";
                        }
                        else
                        {
                            retorno[1] = "Erro ao cadastrar a empresa " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(empresa.Nome.ToLower());
                        }
                    }
                }
                catch (Exception e)
                {
                    retorno[1] = "Erro ao cadastrar a empresa: " + e.Message;
                    retorno[0] = "0";
                }
                conn.Close();
            }

            return retorno;
        }

        internal string[] Delete(Empresa empresa)
        {
            string[] retorno = new string[2];
            strQuery = new StringBuilder();
            strQuery.Append(" DELETE FROM " + table);
            strQuery.Append(" WHERE IdEmpresa = @idEmpresa");
            using (conn = new MySqlConnection(connection))
            {
                conn.Open();
                try
                {
                    using (MySqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = strQuery.ToString();
                        cmd.Parameters.AddWithValue("@idEmpresa", empresa.IdEmpresa);
                        retorno[0] = cmd.ExecuteNonQuery().ToString();
                        if (Convert.ToInt16(retorno[0]) > 0)
                        {
                            retorno[1] = "Empresa " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(empresa.Nome.ToLower()) + " foi excluída";
                        }
                        else
                        {
                            retorno[1] = "Erro ao excluir a empresa " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(empresa.Nome.ToLower());
                        }
                    }
                }
                catch (Exception e)
                {
                    retorno[1] = "Erro ao excluir o item: " + e.Message;
                    retorno[0] = "0";
                }
                conn.Close();

            }
            return retorno;
        }

        public Empresa Carregar(int idEmpresa)
        {
            strQuery = new StringBuilder();
            Empresa empresa = new Empresa();
            strQuery.Append(" SELECT IdEmpresa, IdAgencia, Nome, Fantasia, Endereco, Numero, Complemento, Bairro, Cidade, UF, CEP, Fone1, Fone2, Fone3, idCpf, Insc_rg, Email, Contato, Tipo, Dt_nasc, Ativo, Dt_inc, IdUsuario_inc, Dt_alt, IdUsuario_alt, Dt_excl, IdUsuario_excl FROM " + table);

            strQuery.Append(" WHERE IdEmpresa = @idEmpresa");
            using (conn = new MySqlConnection(connection))
            {
                conn.Open();
                try
                {
                    using (MySqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = strQuery.ToString();
                        cmd.Parameters.AddWithValue("@idEmpresa", idEmpresa);
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                foreach (PropertyInfo propertyInfo in empresa.GetType().GetProperties())
                                {
                                    if (propertyInfo.Name != "Error" && propertyInfo.Name != "Item")
                                        propertyInfo.SetValue(empresa, reader[propertyInfo.Name] == DBNull.Value ? null : reader[propertyInfo.Name], null);
                                    if (propertyInfo.Name == "idCpf")
                                        empresa.idCpf = new CN.StringStatement().FormatDocumento(empresa.idCpf);
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    empresa = new Empresa();
                    MessageBox.Show("Erro ao carregar itens: " + e.Message, "Erro em execução", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                conn.Close();
            }
            return empresa;
        }

        //public List<Empresa> Paginate(int page, int totalPerPage)
        //{
        //    strQuery = new StringBuilder();
        //    List<Empresa> Lista = new List<Empresa>();
        //    strQuery.Append(" SELECT * FROM " + table + " LIMIT " + ((page - 1) * totalPerPage) + "," + totalPerPage);
        //    using (conn = new MySqlConnection(connection))
        //    {
        //        conn.Open();
        //        try
        //        {
        //            using (MySqlCommand cmd = conn.CreateCommand())
        //            {
        //                cmd.CommandType = System.Data.CommandType.Text;
        //                cmd.CommandText = strQuery.ToString();
        //                using (DbDataReader reader = cmd.ExecuteReader())
        //                {
        //                    while (reader.Read())
        //                    {
        //                        Empresa Empresa = new Empresa();
        //                        foreach (PropertyInfo propertyInfo in Empresa.GetType().GetProperties())
        //                        {
        //                            if (propertyInfo.Name != "Error" && propertyInfo.Name != "Item")
        //                                propertyInfo.SetValue(Empresa, reader[propertyInfo.Name] == DBNull.Value ? null : reader[propertyInfo.Name], null);
        //                        }
        //                        Lista.Add(Empresa);
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            Lista = new List<Empresa>();
        //            MessageBox.Show("Erro ao carregar itens: " + e.Message, "Erro em execução", MessageBoxButton.OK, MessageBoxImage.Error);
        //        }
        //        conn.Close();
        //    }
        //    return Lista;
        //}

        //internal int Registro()
        //{
        //    strQuery.Append(" SELECT count(IdEmpresa) FROM " + table);
        //    using (conn = new MySqlConnection(connection))
        //    {
        //        conn.Open();
        //        using (MySqlCommand cmd = conn.CreateCommand())
        //        {
        //            try
        //            {
        //                cmd.CommandType = System.Data.CommandType.Text;
        //                cmd.CommandText = strQuery.ToString();
        //                return Convert.ToInt32(cmd.ExecuteScalar());
        //            }
        //            catch (Exception e)
        //            {
        //                return 0;
        //            }
        //        }
        //    }
        //}
    }
}