﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using SysTur.Model;

namespace SysTur.DAO
{
    public class DAOAbastecimento
    {
        private StringBuilder strQuery = new StringBuilder();
        private string table = "tbabastecimento";
        private MySqlConnection conn = new MySqlConnection();
        private string connection = Properties.Settings.Default.connection;

        public List<Abastecimento> Listar(int? idAgencia = null, int? idCarro = null, int? idMotorista = null, int? idFornecedor = null)
        {
            strQuery = new StringBuilder();
            List<Abastecimento> Lista = new List<Abastecimento>();
            strQuery.Append(" Select * from " + table);
            if (!idAgencia.Equals(null) && !idCarro.Equals(null) && !idMotorista.Equals(null) && !idFornecedor.Equals(null))
                strQuery.Append("WHERE IDABASTECIMENTO IS NOT NULL");
            if (!idAgencia.Equals(null))
            {
                strQuery.Append(" AND IDAGENCIA = @IdAgencia");
            }
            if (!idCarro.Equals(null))
            {
                strQuery.Append(" AND IDCARRO = @IdCarro");
            }
            if (!idMotorista.Equals(null))
            {
                strQuery.Append(" AND IDMOTORISTA= @IdMotorista");
            }
            if (!idFornecedor.Equals(null))
            {
                strQuery.Append(" AND IDFORNECEDOR = @IdFornecedor");
            }

            using (conn = new MySqlConnection(connection))
            {
                conn.Open();
                try
                {
                    using (MySqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = strQuery.ToString();
                        cmd.Parameters.AddWithValue("@IdAgencia", idAgencia);
                        cmd.Parameters.AddWithValue("@IdCarro", idCarro);
                        cmd.Parameters.AddWithValue("@IdMotorista", idMotorista);
                        cmd.Parameters.AddWithValue("@IdFornecedor", idFornecedor);
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Abastecimento Abastecimento = new Abastecimento();
                                foreach (PropertyInfo propertyInfo in Abastecimento.GetType().GetProperties())
                                {
                                    if (propertyInfo.Name != "Error" && propertyInfo.Name != "Item")
                                        propertyInfo.SetValue(Abastecimento, reader[propertyInfo.Name] == DBNull.Value ? null : reader[propertyInfo.Name], null);
                                }
                                Lista.Add(Abastecimento);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Lista = new List<Abastecimento>();
                    MessageBox.Show("Erro ao carregar itens: " + e.Message, "Erro em execução", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                conn.Close();
            }
            return Lista;
        }

        public Abastecimento Carregar(int idAbastecimento)
        {
            strQuery = new StringBuilder();
            Abastecimento boleto = new Abastecimento();
            strQuery.Append(" Select * from " + table);

            strQuery.Append(" WHERE IDABASTECIMENTO = @idAbastecimento");
            using (conn = new MySqlConnection(connection))
            {
                conn.Open();
                try
                {
                    using (MySqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = strQuery.ToString();
                        cmd.Parameters.AddWithValue("@idAbastecimento", idAbastecimento);
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                foreach (PropertyInfo propertyInfo in boleto.GetType().GetProperties())
                                {
                                    if (propertyInfo.Name != "Error" && propertyInfo.Name != "Item")
                                        propertyInfo.SetValue(boleto, reader[propertyInfo.Name] == DBNull.Value ? null : reader[propertyInfo.Name], null);
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    boleto = new Abastecimento();
                    MessageBox.Show("Erro ao carregar itens: " + e.Message, "Erro em execução", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                conn.Close();
            }
            return boleto;
        }
    }
}