﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using SysTur.Model;

namespace SysTur.DAO
{
    public class DAOUsuario
    {
        private StringBuilder strQuery = new StringBuilder();
        private string table = "tbusuarios";
        private MySqlConnection conn = new MySqlConnection();
        private string connection = Properties.Settings.Default.connection;

        public Usuario Login(string nome, string senha)
        {
            Usuario usuario = new Usuario();
            strQuery = new StringBuilder();
            strQuery.Append(" Select * from " + table + " WHERE login = @login AND senha = @senha");
            using (conn = new MySqlConnection(connection))
            {
                conn.Open();
                try
                {
                    using (MySqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = strQuery.ToString();
                        cmd.Parameters.AddWithValue("@login", nome);
                        cmd.Parameters.AddWithValue("@senha", senha);
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                foreach (PropertyInfo propertyInfo in usuario.GetType().GetProperties())
                                {
                                    if (propertyInfo.Name != "Error" && propertyInfo.Name != "Item")
                                        propertyInfo.SetValue(usuario, reader[propertyInfo.Name] == DBNull.Value ? null : reader[propertyInfo.Name], null);
                                }
                            }
                            else
                            {
                                usuario.idUsuario = 0;
                                usuario.Nome = "Usuário não encontrado, verifique suas credenciais.";
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    usuario.idUsuario = 0;
                    usuario.Nome = "Erro em encontrar o usuário! " + e.Message;
                }
                conn.Close();
            }
            return usuario;
        }
    }
}
