﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using SysTur.Model;

namespace SysTur.DAO
{
    public class DAOAgencia
    {
        private StringBuilder strQuery = new StringBuilder();
        private string table = "tbAgencias";
        private MySqlConnection conn = new MySqlConnection();
        private string connection = Properties.Settings.Default.connection;

        public List<Agencia> Listar()
        {
            strQuery = new StringBuilder();
            List<Agencia> Lista = new List<Agencia>();
            strQuery.Append(" Select * from " + table);
            using (conn = new MySqlConnection(connection))
            {
                conn.Open();
                try
                {
                    using (MySqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = strQuery.ToString();
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Agencia Agencia = new Agencia();
                                foreach (PropertyInfo propertyInfo in Agencia.GetType().GetProperties())
                                {
                                    if (propertyInfo.Name != "Error" && propertyInfo.Name != "Item")
                                        propertyInfo.SetValue(Agencia, reader[propertyInfo.Name] == DBNull.Value ? null : reader[propertyInfo.Name], null);
                                }
                                Lista.Add(Agencia);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Lista = new List<Agencia>();
                    MessageBox.Show("Erro ao carregar itens: " + e.Message, "Erro em execução", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                conn.Close();
            }
            return Lista;
        }

        public Agencia Carregar(int idAgencia)
        {
            strQuery = new StringBuilder();
            Agencia boleto = new Agencia();
            strQuery.Append(" Select * from " + table);

            strQuery.Append(" WHERE IdAgencia = @idAgencia");
            using (conn = new MySqlConnection(connection))
            {
                conn.Open();
                try
                {
                    using (MySqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.CommandText = strQuery.ToString();
                        cmd.Parameters.AddWithValue("@idAgencia", idAgencia);
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                foreach (PropertyInfo propertyInfo in boleto.GetType().GetProperties())
                                {
                                    if (propertyInfo.Name != "Error" && propertyInfo.Name != "Item")
                                        propertyInfo.SetValue(boleto, reader[propertyInfo.Name] == DBNull.Value ? null : reader[propertyInfo.Name], null);
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    boleto = new Agencia();
                    MessageBox.Show("Erro ao carregar itens: " + e.Message, "Erro em execução", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                conn.Close();
            }
            return boleto;
        }
    }
}
