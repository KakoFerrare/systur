﻿using Notifications.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Windows;
using SysTur.Model;
using SysTur.ModelView;

namespace SysTur.View.Cadastro.Cliente
{
    /// <summary>
    /// Interaction logic for CreateCliente.xaml
    /// </summary>
    public partial class ManageCliente : Window
    {
        ICollection<System.ComponentModel.DataAnnotations.ValidationResult> results = null;
        private MVCliente mvCliente;
        public bool UpdateParentWindow;

        public bool UpdateGetSet
        {
            set
            {
                UpdateParentWindow = value;
            }
            get
            {
                return UpdateParentWindow;
            }
        }
        public ManageCliente()
        {
            InitializeComponent();
            mvCliente = new MVCliente();
            this.DataContext = mvCliente;
        }

        public ManageCliente(Empresa empresa)
        {
            InitializeComponent();
            mvCliente = new MVCliente(empresa.IdEmpresa);
            this.ContentTitle.Content = "Editar " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(empresa.Nome.ToLower());
            this.DataContext = mvCliente;

        }

        private void Manage(object sender, RoutedEventArgs e)
        {
            var notificationManager = new NotificationManager();
            if (Validator.TryValidateObject(mvCliente.Empresa, new ValidationContext(mvCliente.Empresa), results, true))
            {
                if (new CN.CNFuncoes().ValidarDocumento(mvCliente.Empresa.idCpf))
                {
                    string[] retorno = new CN.CNEmpresa().Manage(mvCliente.Empresa);
                    
                    if (Convert.ToInt16(retorno[0]) > 0)
                    {
                        //Cliente.Listar();
                        //dataGridEmpresa.ItemsSource = Cliente.Lista_Empresa;

                        notificationManager.Show(new NotificationContent
                        {
                            Title = "Sucesso",
                            Message = retorno[1],
                            Type = NotificationType.Information

                        });
                        UpdateGetSet = true;
                        this.Close();
                    }
                    else
                    {
                        notificationManager.Show(new NotificationContent
                        {
                            Title = "Erro",
                            Message = retorno[1],
                            Type = NotificationType.Error
                        });
                    }
                }
                else
                {
                    notificationManager.Show(new NotificationContent
                    {
                        Title = "Erro",
                        Message = "Erro em validar o documento.",
                        Type = NotificationType.Error
                    });
                }
            }
        }

        private void Cancelar(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ConsultarCep(object sender, RoutedEventArgs e)
        {
            //Link de exemplo de adicionar um serviço por referencia:
            //https://thiagoborges.net.br/como-consumir-um-web-service-no-c/
            try
            {
                var correios = new Correios.AtendeClienteClient();
                var consulta = correios.consultaCEP(mvCliente.Empresa.Cep);
                if (mvCliente.Empresa.Cep.Length == 8)
                    this.CEP.Text = mvCliente.Empresa.Cep.Insert(5, "-");
                this.Endereco.Text = consulta.end;
                this.Bairro.Text = consulta.bairro;
                this.Cidade.Text = consulta.cidade;
                this.Estado.Text = consulta.uf;
            }
            catch (Exception exc)
            {
                var notificationManager = new NotificationManager();
                notificationManager.Show(new NotificationContent
                {
                    Title = "Erro",
                    Message = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(exc.Message.ToString().ToLower()),
                    Type = NotificationType.Error
                });
            }


        }
    }
}
