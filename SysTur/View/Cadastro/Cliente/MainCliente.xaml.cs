﻿using Notifications.Wpf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SysTur.Model;
using SysTur.ModelView;

namespace SysTur.View.Cadastro.Cliente
{
    /// <summary>
    /// Interaction logic for MainCliente.xaml
    /// </summary>
    public partial class MainCliente : UserControl
    {
        private List<Empresa> FilterList;
        const int TotalPerPage = 12;
        private int currentPage;
        private int number;

        private MVCliente Cliente;
        public MainCliente()
        {
            Cliente = new MVCliente();
            Cliente.Listar();
            this.DataContext = Cliente;
            FilterList = Cliente.Lista_Empresa;
            InitializeComponent();
            Binding(TotalPerPage, 1);
        }
        private void Cadastrar(object sender, RoutedEventArgs e)
        {
            ManageCliente manage = new ManageCliente();
            manage.Show();
            if (manage.UpdateParentWindow == true)
            {
                dataGridEmpresa.ItemsSource = null;
                Cliente.Listar();
                this.FilterList = Cliente.Lista_Empresa;
                dataGridEmpresa.ItemsSource = Cliente.Lista_Empresa;
                Binding(this.number, this.currentPage);
            }
        }

        private void Edit(object sender, RoutedEventArgs e)
        {
            Empresa empresa = (Empresa)dataGridEmpresa.SelectedItem;
            ManageCliente manage = new ManageCliente(empresa);
            manage.ShowDialog();
            if (manage.UpdateParentWindow == true)
            {
                dataGridEmpresa.ItemsSource = null;
                int index = Cliente.Lista_Empresa.IndexOf(empresa);
                Cliente.Lista_Empresa.RemoveAt(index);
                Cliente.Lista_Empresa.Insert(index, new CN.CNEmpresa().Carregar(empresa.IdEmpresa));
                dataGridEmpresa.ItemsSource = Cliente.Lista_Empresa;
                Binding(this.number, this.currentPage);

            }
        }

        private void Delete(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Deseja mesmo excluir o cliente?", "Confirmação de Exclusão", System.Windows.MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                Empresa empresa = (Empresa)dataGridEmpresa.SelectedItem;
                ConfirmDelete(empresa);
            }
        }

        private void ConfirmDelete(Empresa empresa)
        {
            //https://github.com/Federerer/Notifications.Wpf
            var notificationManager = new NotificationManager();
            string[] retorno = new CN.CNEmpresa().Delete(empresa);
            if (Convert.ToInt16(retorno[0]) > 0)
            {
                dataGridEmpresa.ItemsSource = null;
                int index = Cliente.Lista_Empresa.IndexOf(empresa);
                Cliente.Lista_Empresa.RemoveAt(index);
                dataGridEmpresa.ItemsSource = Cliente.Lista_Empresa;
                Binding(this.number, this.currentPage);
                notificationManager.Show(new NotificationContent
                {
                    Title = "Sucesso",
                    Message = retorno[1],
                    Type = NotificationType.Information
                });
            }
            else
            {
                notificationManager.Show(new NotificationContent
                {
                    Title = "Erro",
                    Message = retorno[1],
                    Type = NotificationType.Error
                });
            }
        }

        private void Binding(int number, int currentPage)
        {
            this.number = number;
            this.currentPage = currentPage;

            int totalRegister = Cliente.Lista_Empresa.Count();
            int pageSize = 0;
            if (totalRegister % number == 0)
            {
                pageSize = totalRegister / number;
            }
            else
            {
                pageSize = totalRegister / number + 1;
            }

            this.tbkTotal.Text = pageSize.ToString();
            tbkCurrentsize.Text = currentPage.ToString();
            dataGridEmpresa.ItemsSource = Cliente.Lista_Empresa.Take(number * currentPage).Skip(number * (currentPage - 1)).ToList();
        }

        private void FirstPage(object sender, RoutedEventArgs e)
        {
            int currentPage = int.Parse(tbkCurrentsize.Text);
            if (currentPage > 1)
            {
                Binding(TotalPerPage, 1);
            }
        }

        private void LastPage(object sender, RoutedEventArgs e)
        {
            int currentPage = int.Parse(tbkCurrentsize.Text);
            if (currentPage < Convert.ToInt32(this.tbkTotal.Text))
            {
                Binding(TotalPerPage, Convert.ToInt32(this.tbkTotal.Text));
            }
        }

        private void NextPage(object sender, RoutedEventArgs e)
        {
            int total = int.Parse(tbkTotal.Text);
            int currentPage = int.Parse(tbkCurrentsize.Text);
            if (currentPage < total)
            {
                Binding(TotalPerPage, currentPage + 1);
            }
        }

        private void PrevPage(object sender, RoutedEventArgs e)
        {
            int total = int.Parse(tbkTotal.Text);
            int currentPage = int.Parse(tbkCurrentsize.Text);
            if (currentPage <= total && currentPage > 1)
            {
                Binding(TotalPerPage, currentPage - 1);
            }
        }

        private void GoPage(object sender, RoutedEventArgs e)
        {
            int pageGoNum;
            if (tbxPageNum.Text != null && int.TryParse(tbxPageNum.Text, out pageGoNum))
            {
                int pageNum = int.Parse(tbxPageNum.Text);
                int total = int.Parse(tbkTotal.Text);
                if (pageNum >= 1 && pageNum <= total)
                {
                    Binding(TotalPerPage, pageNum);
                }
            }

        }

        private void Filtar(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(Filtro.Text))
            {
                Cliente.Lista_Empresa = this.FilterList;
            }
            else
            {
                Cliente.Lista_Empresa = this.FilterList;
                Cliente.Lista_Empresa = Cliente.Lista_Empresa.Where(x => (x.Nome != null && x.Nome.ToLower().Contains(Filtro.Text.ToLower())) ||
                (x.Fantasia != null && x.Fantasia.ToLower().Contains(Filtro.Text.ToLower()))||
                (x.IdAgencia == (Cliente.Lista_Agencia.Where(a => a.Nome.ToLower() == Filtro.Text.ToLower()).Select(a => a.IdAgencia).FirstOrDefault())) ||
                (x.Insc_rg != null && x.Insc_rg.ToLower().Contains(Filtro.Text.ToLower()))).ToList();
            }
            dataGridEmpresa.ItemsSource = Cliente.Lista_Empresa;
            Binding(this.number, 1);
        }

        private void Refresh(object sender, KeyEventArgs e)
        {
            if (string.IsNullOrEmpty(Filtro.Text))
            {
                Cliente.Listar();
                dataGridEmpresa.ItemsSource = Cliente.Lista_Empresa;
                Binding(this.number, 1);
            }
        }
    }
}
