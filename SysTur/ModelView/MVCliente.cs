﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SysTur.Model;

namespace SysTur.ModelView
{
    public class MVCliente
    {
        public List<Empresa> Lista_Empresa { get; set; }
        public Empresa Empresa { get; set; }
        public Agencia Agencia { get; set; }
        public List<Agencia> Lista_Agencia { get; set; }

        public MVCliente()
        {
            Empresa = new Empresa();
            Lista_Agencia = new CN.CNAgencia().Listar();
        }
        public MVCliente(int idEmpresa)
        {
            Empresa = new CN.CNEmpresa().Carregar(idEmpresa);
            Lista_Agencia = new CN.CNAgencia().Listar();
            Agencia = new CN.CNAgencia().Carregar((int)Empresa.IdAgencia);
        }
        public void Listar()
        {
            Lista_Empresa = new CN.CNEmpresa().Listar();
            Lista_Agencia = new CN.CNAgencia().Listar();

        }
    }
}
