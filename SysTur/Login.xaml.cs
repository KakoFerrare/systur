﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SysTur.Model;

namespace SysTur
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
        }

        private void LoginApp(object sender, RoutedEventArgs e)
        {
            Usuario usuario = new CN.CNUsuario().Login(inputLogin.Text, inputPassword.Password);
            if(usuario.idUsuario > 0)
            {
                MainWindow main = new MainWindow();
                this.Close();
                main.Show();
            }
            else
            {
                ErrorMessagemLogin.Text = usuario.Nome;
            }

        }

        private void Logout(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
