﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SysTur.View.Cadastro.Cliente;

namespace SysTur
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {
            
            InitializeComponent();
            this.ContentTitle.Content = "Bem-vindo";
           
        }
        //private void MenuItem_MouseEnter(object sender, MouseEventArgs e)
        //{
        //    MenuItem mi = sender as MenuItem;
        //    mi.IsSubmenuOpen = true;
        //}

        //private void MenuItem_MouseLeave(object sender, MouseEventArgs e)
        //{
        //    MenuItem mi = sender as MenuItem;
        //    mi.IsSubmenuOpen = false;
        //}


        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MainCliente MainCliente = new MainCliente();
            this.ContentTitle.Content = "Clientes";
            this.MainContent.Content = MainCliente;
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
            Application.Current.Shutdown();
        }

        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MainCliente MainCliente = new MainCliente();
            this.ContentTitle.Content = "Bem-vindo";
            this.MainContent.Content = null;
        }
    }
}
